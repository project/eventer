CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module replaces all hook/alter functions calls with event subscribers
calls, and it dispatches global events too.

After installing it all hooks and alters functions will be executed with
system events, this allows remove, overwrite, or replace module
implementations. This enables general events too, not linked to modules, for
new hooks/alters implementations, now with an event approach.

  * For a full description of the module, visit the project page:
     https://www.drupal.org/project/eventer

  * To submit bug reports and feature suggestions, or track changes:
     https://www.drupal.org/project/issues/eventer

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

 * You could use: composer require drupal/eventer

CONFIGURATION
-------------

  * After enabling this module all hooks and alters will be executed in the
    same way that without the module, but using events. You not must-see any
    site change.

  * The module has a block to see events subscribers added to build the current
    page, it's a nice tool to debug events.

FAQ
---

  * How is the event key naming?

  - Event prefixes:

  EventerEvents::MODULE_HOOK = 'eventer.module.'
  EventerEvents::MODULE_HOOK_ALTER = 'eventer.module.alter.'
  EventerEvents::THEME_HOOK_ALTER = 'eventer.theme.alter.'

  - Variable naming:
  $module: Module machine name.
  $hook: Hook base type, f.e. 'entity_field_access'
  $type: Same as $hook.
  $extra_type: Same as $hook.
  $theme_key: Theme machine name.

  - Event naming:

  EventerEvents::MODULE_HOOK . $module . '.' . $hook
  EventerEvents::MODULE_HOOK . $hook
  EventerEvents::MODULE_HOOK_ALTER . $type
  EventerEvents::MODULE_HOOK_ALTER . $module . '.' . $type
  EventerEvents::MODULE_HOOK_ALTER . $extra_type
  EventerEvents::MODULE_HOOK_ALTER . $module . '.' . $extra_type

  EventerEvents::THEME_HOOK_ALTER . $type
  EventerEvents::THEME_HOOK_ALTER . $theme_key . '.' . $type
  EventerEvents::THEME_HOOK_ALTER . $extra_type
  EventerEvents::THEME_HOOK_ALTER . $theme_key . '.' . $extra_type

  - Event examples:

  Event key: eventer.module.alter.content_translation.language_types_info
  Hook: language_types_info
  Module: content_translation
  Prefix: EventerEvents::MODULE_HOOK_ALTER
  Hook function: content_translation_language_types_info_alter
  Not hook event key: eventer.module.alter.language_types_info

  Event key: eventer.module.charts.views_pre_view
  Hook: views_pre_view
  Module: charts
  Prefix: EventerEvents::MODULE_HOOK
  Hook function: charts_views_pre_view
  Not hook event key: eventer.module.views_pre_view

  Event key: eventer.module.group.entity_field_access
  Hook: entity_field_access
  Module: group
  Prefix: EventerEvents::MODULE_HOOK
  Hook function: group_entity_field_access
  Not hook event key: eventer.module.entity_field_access

  Event key: eventer.module.alter.content_translation.module_implements
  Hook: module_implements
  Module: content_translation
  Prefix: EventerEvents::MODULE_HOOK_ALTER
  Hook function: content_translation_module_implements_alter
  Not hook event key: eventer.module.alter.module_implements

  Event key: eventer.module.alter.group.form
  Hook: form
  Module: N/A
  Prefix: EventerEvents::MODULE_HOOK_ALTER
  Hook function: N/A
  Not hook event key: N/A

  Event key: eventer.theme.alter.mytheme.page_attachments
  Hook: page_attachments
  Theme: mytheme
  Prefix: EventerEvents::THEME_HOOK_ALTER
  Hook function: mytheme_page_attachments_alter
  Not hook event key: eventer.theme.alter.page_attachments

MAINTAINERS
-----------

Current maintainers:

 * Pedro Pelaez (psf_) - https://www.drupal.org/u/psf_

This project has been sponsored by:

 * Front.id
   Front ID is a distributed web development company of Drupal experts, but not
   limited to it. Our services include Frontend and Backend development, Drupal
   consultancy and Drupal training.
