<?php

namespace Drupal\eventer\Decorator;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Theme\ActiveTheme;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManager;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\eventer\Event\AlterEvent;
use Drupal\eventer\EventerEvents;
use Drupal\eventer\EventSubscriber\LegacyHookSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ModuleHandlerDecorator.
 *
 * Translate all module hooks to events.
 *
 * @package Drupal\eventer\Decorator
 */
class ThemeManagerDecorator extends ThemeManager {

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a new ThemeManager object.
   *
   * @param string $root
   *   The app root.
   * @param \Drupal\Core\Theme\ThemeNegotiatorInterface $theme_negotiator
   *   The theme negotiator.
   * @param \Drupal\Core\Theme\ThemeInitializationInterface $theme_initialization
   *   The theme initialization.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   */
  public function __construct(
    $root,
    ThemeNegotiatorInterface $theme_negotiator,
    ThemeInitializationInterface $theme_initialization,
    ModuleHandlerInterface $module_handler,
    EventDispatcherInterface $eventDispatcher
  ) {
    parent::__construct($root, $theme_negotiator, $theme_initialization, $module_handler);
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Should we cache some of these information?
   */
  public function alterForTheme(ActiveTheme $theme, $type, &$data, &$context1 = NULL, &$context2 = NULL) {
    // Most of the time, $type is passed as a string, so for performance,
    // normalize it to that. When passed as an array, usually the first item in
    // the array is a generic type, and additional items in the array are more
    // specific variants of it, as in the case of array('form', 'form_FORM_ID').
    if (is_array($type)) {
      $extra_types = $type;
      $type = array_shift($extra_types);
      // Allow if statements in this function to use the faster isset() rather
      // than !empty() both when $type is passed as a string, or as an array
      // with one item.
      if (empty($extra_types)) {
        unset($extra_types);
      }
    }

    $theme_keys = array_keys($theme->getBaseThemeExtensions());
    $theme_keys[] = $theme->getName();
    $eventsAlter = [];
    $eventsAlter[EventerEvents::THEME_HOOK_ALTER . $type] = [
      'module' => NULL,
      'function' => NULL,
      'hook' => $type . '_alter',
    ];
    foreach ($theme_keys as $theme_key) {
      $function = $theme_key . '_' . $type . '_alter';
      if (function_exists($function)) {
        $eventKey = EventerEvents::THEME_HOOK_ALTER . $theme_key . '.' . $type;
        $this->registerAlterEvent($eventKey);
        $eventsAlter[$eventKey] = [
          'module' => $theme_key,
          'function' => $function,
          'hook' => $type . '_alter',
        ];
      }
      if (isset($extra_types)) {
        foreach ($extra_types as $extra_type) {
          $eventsAlter[EventerEvents::THEME_HOOK_ALTER . $extra_type] = [
            'module' => NULL,
            'function' => NULL,
            'hook' => $extra_type . '_alter',
          ];
          $function = $theme_key . '_' . $extra_type . '_alter';
          if (function_exists($function)) {
            $eventKey = EventerEvents::THEME_HOOK_ALTER . $theme_key . '.' . $extra_type;
            $this->registerAlterEvent($eventKey);
            $eventsAlter[$eventKey] = [
              'module' => $theme_key,
              'function' => $function,
              'hook' => $extra_type . '_alter',
            ];
          }
        }
      }
    }

    foreach ($eventsAlter as $eventName => $eventAlter) {
      $eventParam = new AlterEvent($eventAlter['hook'], $eventAlter['module'], $eventAlter['function'], $data, $context1, $context2);
      // This allow an easiest debugging.
      $eventParam->setKey($eventName);
      /** @var \Drupal\eventer\Event\AlterEvent $event */
      $event = $this->eventDispatcher->dispatch($eventName, $eventParam);
      $data = $event->getData();
      $context1 = $event->getContext1();
      $context2 = $event->getContext2();
      unset($eventParam);
    }
  }

  /**
   * Register an alter default handler.
   *
   * @param string $eventKey
   *   The event key.
   */
  protected function registerAlterEvent($eventKey) {
    $listeners = $this->eventDispatcher->getListeners($eventKey);
    $hasLegacy = FALSE;
    foreach ($listeners as $listener) {
      if ($listener[0] instanceof LegacyHookSubscriber) {
        $hasLegacy = TRUE;
        break;
      }
    }
    unset($listeners);
    if (!$hasLegacy) {
      $subscriber = new LegacyHookSubscriber();
      $this->eventDispatcher->addListener($eventKey, [$subscriber, 'alter'], 0);
    }
  }

}
