<?php

namespace Drupal\eventer\Decorator;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\eventer\Event\HookEvent;
use Drupal\eventer\Event\AlterEvent;
use Drupal\eventer\EventerEvents;
use Drupal\eventer\EventSubscriber\LegacyHookSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ModuleHandlerDecorator.
 *
 * Translate all module hooks to events.
 *
 * @package Drupal\eventer\Decorator
 */
class ModuleHandlerDecorator extends ModuleHandler {

  use DependencySerializationTrait;

  /**
   * List of alter events keyed by event name(s).
   *
   * @var array
   */
  protected $eventsAlter;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a ModuleHandler object.
   *
   * @param string $root
   *   The app root.
   * @param array $module_list
   *   An associative array whose keys are the names of installed modules and
   *   whose values are Extension class parameters. This is normally the
   *   %container.modules% parameter being set up by DrupalKernel.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend for storing module hook implementation information.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   *
   * @see \Drupal\Core\DrupalKernel
   * @see \Drupal\Core\CoreServiceProvider
   */
  public function __construct(
    $root,
    array $module_list,
    CacheBackendInterface $cache_backend,
    EventDispatcherInterface $eventDispatcher
  ) {
    parent::__construct($root, $module_list, $cache_backend);
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($module, $hook, array $args = []) {
    if (!$this->implementsHook($module, $hook)) {
      return;
    }
    // Register legacy listener if not registered.
    $this->registerHookEvents([$module], $hook);
    $function = $module . '_' . $hook;
    $eventParam = new HookEvent($hook, $module, $function, $args);
    // This allow an easiest debugging.
    $eventParam->setKey(EventerEvents::MODULE_HOOK . $module . '.' . $hook);
    /** @var \Drupal\eventer\Event\HookEvent $event */
    $event = $this->eventDispatcher->dispatch($eventParam->getKey(), $eventParam);
    return $event->getResult();
  }

  /**
   * {@inheritdoc}
   */
  public function invokeAll($hook, array $args = []) {
    $return = [];
    $implementations = $this->getImplementations($hook);
    // Register legacy listeners if not registered.
    $this->registerHookEvents($implementations, $hook);
    // Call hooks by events.
    foreach ($implementations as $module) {
      // Legacy call.
      $function = $module . '_' . $hook;
      $eventParam = new HookEvent($hook, $module, $function, $args);
      // This allow an easiest debugging.
      $eventParam->setKey(EventerEvents::MODULE_HOOK . $module . '.' . $hook);
      /** @var \Drupal\eventer\Event\HookEvent $event */
      $event = $this->eventDispatcher->dispatch($eventParam->getKey(), $eventParam);
      $result = $event->getResult();
      if (isset($result) && is_array($result)) {
        $return = NestedArray::mergeDeep($return, $result);
      }
      elseif (isset($result)) {
        $return[] = $result;
      }
    }
    // Events call.
    $eventParam = new HookEvent($hook, NULL, NULL, $args);
    // This allow an easiest debugging.
    $eventParam->setKey(EventerEvents::MODULE_HOOK . $hook);
    /** @var \Drupal\eventer\Event\HookEvent $event */
    $event = $this->eventDispatcher->dispatch($eventParam->getKey(), $eventParam);
    $result = $event->getResult();
    if (isset($result) && is_array($result)) {
      $return = NestedArray::mergeDeep($return, $result);
    }
    elseif (isset($result)) {
      $return[] = $result;
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function alter($type, &$data, &$context1 = NULL, &$context2 = NULL) {
    // Most of the time, $type is passed as a string, so for performance,
    // normalize it to that. When passed as an array, usually the first item in
    // the array is a generic type, and additional items in the array are more
    // specific variants of it, as in the case of array('form', 'form_FORM_ID').
    if (is_array($type)) {
      $cid = implode(',', $type);
      $extra_types = $type;
      $type = array_shift($extra_types);
      // Allow if statements in this function to use the faster isset() rather
      // than !empty() both when $type is passed as a string, or as an array
      // with one item.
      if (empty($extra_types)) {
        unset($extra_types);
      }
    }
    else {
      $cid = $type;
    }

    // Some alter hooks are invoked many times per page request, so store the
    // list of functions to call, and on subsequent calls, iterate through them
    // quickly.
    $this->eventsAlter[$cid][EventerEvents::MODULE_HOOK_ALTER . $type] = [
      'module' => NULL,
      'function' => NULL,
      'hook' => $type . '_alter',
    ];
    if (!isset($this->alterFunctions[$cid])) {
      $this->alterFunctions[$cid] = [];
      $hook = $type . '_alter';
      $modules = $this->getImplementations($hook);
      if (!isset($extra_types)) {
        // For the more common case of a single hook, we do not need to call
        // function_exists(), since $this->getImplementations() returns only
        // modules with implementations.
        foreach ($modules as $module) {
          $this->alterFunctions[$cid][] = $module . '_' . $hook;
          $this->registerAlterEvent(
            $cid,
            EventerEvents::MODULE_HOOK_ALTER . $module . '.' . $type,
            $module,
            $module . '_' . $hook,
            $hook
          );
        }
      }
      else {
        // For multiple hooks, we need $modules to contain every module that
        // implements at least one of them.
        $extra_modules = [];
        foreach ($extra_types as $extra_type) {
          $extra_modules = array_merge($extra_modules, $this->getImplementations($extra_type . '_alter'));
        }
        // If any modules implement one of the extra hooks that do not implement
        // the primary hook, we need to add them to the $modules array in their
        // appropriate order. $this->getImplementations() can only return
        // ordered implementations of a single hook. To get the ordered
        // implementations of multiple hooks, we mimic the
        // $this->getImplementations() logic of first ordering by
        // $this->getModuleList(), and then calling
        // $this->alter('module_implements').
        if (array_diff($extra_modules, $modules)) {
          // Merge the arrays and order by getModuleList().
          $modules = array_intersect(array_keys($this->moduleList), array_merge($modules, $extra_modules));
          // Since $this->getImplementations() already took care of loading the
          // necessary include files, we can safely pass FALSE for the array
          // values.
          $implementations = array_fill_keys($modules, FALSE);
          // Let modules adjust the order solely based on the primary hook. This
          // ensures the same module order regardless of whether this if block
          // runs. Calling $this->alter() recursively in this way does not
          // result in an infinite loop, because this call is for a single
          // $type, so we won't end up in this code block again.
          $this->alter('module_implements', $implementations, $hook);
          $modules = array_keys($implementations);
        }
        foreach ($modules as $module) {
          // Since $modules is a merged array, for any given module, we do not
          // know whether it has any particular implementation, so we need a
          // function_exists().
          $function = $module . '_' . $hook;
          if (function_exists($function)) {
            $this->alterFunctions[$cid][] = $function;
            $this->registerAlterEvent(
              $cid,
              EventerEvents::MODULE_HOOK_ALTER . $module . '.' . $type,
              $module,
              $function,
              $hook
            );
          }
          $this->eventsAlter[EventerEvents::MODULE_HOOK_ALTER . $extra_type] = [
            'module' => NULL,
            'function' => NULL,
            'hook' => $hook,
          ];
          foreach ($extra_types as $extra_type) {
            $function = $module . '_' . $extra_type . '_alter';
            if (function_exists($function)) {
              $this->alterFunctions[$cid][] = $function;
              $this->registerAlterEvent(
                $cid,
                EventerEvents::MODULE_HOOK_ALTER . $module . '.' . $extra_type,
                $module,
                $function,
                $hook
              );
            }
          }
        }
      }
    }

    foreach ($this->eventsAlter[$cid] as $eventName => $eventAlter) {
      $eventParam = new AlterEvent($eventAlter['hook'], $eventAlter['module'], $eventAlter['function'], $data, $context1, $context2);
      // This allow an easiest debugging.
      $eventParam->setKey($eventName);
      /** @var \Drupal\eventer\Event\AlterEvent $event */
      $event = $this->eventDispatcher->dispatch($eventName, $eventParam);
      $data = $event->getData();
      $context1 = $event->getContext1();
      $context2 = $event->getContext2();
      unset($eventParam);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function resetImplementations() {
    parent::resetImplementations();
    $this->eventsAlter = NULL;
  }

  /**
   * Register, if not is registered, a hook for each module implementation.
   *
   * Each hook only have one implementation in each module.
   *
   * @param array $implementations
   *   Modules that implements the hook.
   * @param string $hook
   *   The hook.
   */
  protected function registerHookEvents(array $implementations, $hook) {
    foreach ($implementations as $module) {
      $eventKey = EventerEvents::MODULE_HOOK . $module . '.' . $hook;
      $listeners = $this->eventDispatcher->getListeners($eventKey);
      $hasLegacy = FALSE;
      foreach ($listeners as $listener) {
        if ($listener[0] instanceof LegacyHookSubscriber) {
          $hasLegacy = TRUE;
          break;
        }
      }
      unset($listeners);
      if (!$hasLegacy) {
        $subscriber = new LegacyHookSubscriber();
        $this->eventDispatcher->addListener($eventKey, [$subscriber, 'execute'], 0);
      }
    }
  }

  /**
   * Register, if not is registered, a legacy alter event handler.
   *
   * @param string $cid
   *   Current alter list.
   * @param string $eventKey
   *   Event key.
   * @param string $module
   *   Module.
   * @param string $function
   *   Function.
   * @param string $hook
   *   Hook.
   */
  protected function registerAlterEvent($cid, $eventKey, $module, $function, $hook) {
    if (!isset($this->eventsAlter[$cid])) {
      $this->eventsAlter[$cid] = [];
    }
    $this->eventsAlter[$cid][$eventKey] = [
      'module' => $module,
      'function' => $function,
      'hook' => $hook,
    ];
    $listeners = $this->eventDispatcher->getListeners($eventKey);
    $hasLegacy = FALSE;
    foreach ($listeners as $listener) {
      if ($listener[0] instanceof LegacyHookSubscriber) {
        $hasLegacy = TRUE;
        break;
      }
    }
    unset($listeners);
    if (!$hasLegacy) {
      $subscriber = new LegacyHookSubscriber();
      $this->eventDispatcher->addListener($eventKey, [$subscriber, 'alter'], 0);
    }
  }

}
