<?php

namespace Drupal\eventer\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Defines an hook call replacement event.
 */
class HookEvent extends Event {

  /**
   * Event key.
   *
   * @var string
   */
  protected $key;

  /**
   * Hook name.
   *
   * @var string
   */
  protected $hook;

  /**
   * Module.
   *
   * @var string
   */
  protected $module;

  /**
   * Hook implementation name.
   *
   * @var string
   */
  protected $function;

  /**
   * Hook arguments.
   *
   * @var array
   */
  protected $params;

  /**
   * Hook result.
   *
   * @var mixed
   */
  protected $result;

  /**
   * Constructs a new AvailableCountriesEvent object.
   *
   * @param string $hook
   *   Hook key.
   * @param string $module
   *   Module name.
   * @param string $function
   *   Hook implementation name.
   * @param array $params
   *   Hook implementation parameters.
   */
  public function __construct($hook, $module, $function, array $params = []) {
    $this->hook = $hook;
    $this->module = $module;
    $this->function = $function;
    $this->params = $params;
  }

  /**
   * Event key.
   *
   * @return string
   *   Event key.
   */
  public function getKey(): string {
    return $this->key;
  }

  /**
   * Event key.
   *
   * @param string $key
   *   Event key.
   */
  public function setKey(string $key): void {
    $this->key = $key;
  }

  /**
   * Get result.
   *
   * @return mixed
   *   Result
   */
  public function getResult() {
    return $this->result;
  }

  /**
   * Set result.
   *
   * @param mixed $result
   *   Result.
   */
  public function setResult($result): void {
    $this->result = $result;
  }

  /**
   * Get module.
   *
   * @return string
   *   Module.
   */
  public function getModule(): string {
    return $this->module;
  }

  /**
   * Set module.
   *
   * @param string $module
   *   Module.
   */
  public function setModule(string $module): void {
    $this->module = $module;
  }

  /**
   * Get function.
   *
   * @return string
   *   Function.
   */
  public function getFunction(): string {
    return $this->function;
  }

  /**
   * Set function.
   *
   * @param string $function
   *   Function.
   */
  public function setFunction(string $function): void {
    $this->function = $function;
  }

  /**
   * Get hook.
   *
   * @return string
   *   Hook.
   */
  public function getHook(): string {
    return $this->hook;
  }

  /**
   * Set hook.
   *
   * @param string $hook
   *   Hook.
   */
  public function setHook(string $hook): void {
    $this->hook = $hook;
  }

  /**
   * Get parameters.
   *
   * @return array
   *   Parameters.
   */
  public function getParams(): array {
    return $this->params;
  }

  /**
   * Set parameters.
   *
   * @param array $params
   *   Parameters.
   */
  public function setParams(array $params): void {
    $this->params = $params;
  }

}
