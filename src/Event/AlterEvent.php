<?php

namespace Drupal\eventer\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Defines an alter call replacement event.
 */
class AlterEvent extends Event {

  /**
   * Event key.
   *
   * @var string
   */
  protected $key;

  /**
   * Hook name.
   *
   * @var string
   */
  protected $hook;

  /**
   * Module.
   *
   * @var string
   */
  protected $module;

  /**
   * Hook implementation name.
   *
   * @var string
   */
  protected $function;

  /**
   * Data.
   *
   * @var mixed
   */
  protected $data;

  /**
   * Context 1.
   *
   * @var mixed
   */
  protected $context1;

  /**
   * Context 2.
   *
   * @var mixed
   */
  protected $context2;

  /**
   * Constructs a new AvailableCountriesEvent object.
   *
   * @param string $hook
   *   Hook key.
   * @param string $module
   *   Module name.
   * @param string $function
   *   Hook implementation name.
   * @param mixed $data
   *   Data.
   * @param mixed $context1
   *   Context 1.
   * @param mixed $context2
   *   Context 2.
   */
  public function __construct($hook, $module, $function, $data, $context1 = NULL, $context2 = NULL) {
    $this->hook = $hook;
    $this->module = $module;
    $this->function = $function;
    $this->data = $data;
    $this->context1 = $context1;
    $this->context2 = $context2;
  }

  /**
   * Event key.
   *
   * @return string
   *   Event key.
   */
  public function getKey(): string {
    return $this->key;
  }

  /**
   * Event key.
   *
   * @param string $key
   *   Event key.
   */
  public function setKey(string $key): void {
    $this->key = $key;
  }

  /**
   * Data.
   *
   * @return mixed
   *   Data.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Data.
   *
   * @param mixed $data
   *   Data.
   */
  public function setData($data): void {
    $this->data = $data;
  }

  /**
   * Context 1.
   *
   * @return mixed
   *   Context 1.
   */
  public function getContext1() {
    return $this->context1;
  }

  /**
   * Context 1.
   *
   * @param mixed $context1
   *   Context 1.
   */
  public function setContext1($context1): void {
    $this->context1 = $context1;
  }

  /**
   * Context 2.
   *
   * @return mixed
   *   Context 2.
   */
  public function getContext2() {
    return $this->context2;
  }

  /**
   * Context 2.
   *
   * @param mixed $context2
   *   Context 2.
   */
  public function setContext2($context2): void {
    $this->context2 = $context2;
  }

  /**
   * Module.
   *
   * @return string
   *   Module.
   */
  public function getModule(): string {
    return $this->module;
  }

  /**
   * Module.
   *
   * @param string $module
   *   Module.
   */
  public function setModule(string $module): void {
    $this->module = $module;
  }

  /**
   * Function.
   *
   * @return string
   *   Function.
   */
  public function getFunction(): string {
    return $this->function;
  }

  /**
   * Function.
   *
   * @param string $function
   *   Function.
   */
  public function setFunction(string $function): void {
    $this->function = $function;
  }

  /**
   * Hook.
   *
   * @return string
   *   Hook.
   */
  public function getHook(): string {
    return $this->hook;
  }

  /**
   * Hook.
   *
   * @param string $hook
   *   Hook.
   */
  public function setHook(string $hook): void {
    $this->hook = $hook;
  }

}
