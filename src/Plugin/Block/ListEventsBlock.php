<?php

namespace Drupal\eventer\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a 'ListEventsBlock' block.
 *
 * @Block(
 *  id = "list_events_block",
 *  admin_label = @Translation("List events block"),
 * )
 */
class ListEventsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Symfony\Component\EventDispatcher\EventDispatcherInterface definition.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The kill switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->eventDispatcher = $container->get('event_dispatcher');
    $instance->killSwitch = $container->get('page_cache_kill_switch');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $output = [];
    $output['#theme'] = 'list_events_block';

    $events = $this->eventDispatcher->getListeners();
    $header = [$this->t('Event key'), $this->t('Listeners')];
    $data = [];
    foreach ($events as $eventKey => $listeners) {
      $imps = [];
      foreach ($listeners as $listener) {
        $pri = $this->eventDispatcher->getListenerPriority($eventKey, $listener);
        $imps[] = '[Pri. ' . $pri . '] ' . get_class($listener[0]) . '::' . $listener[1];
      }
      $imps = implode('<br>', $imps);
      $data[] = [$eventKey, new FormattableMarkup($imps, [])];
    }

    $output[] = [
      '#theme' => 'table',
      // '#cache' => ['disabled' => TRUE],
      '#caption' => $this->t('Detected @cnt events types.', [
        '@cnt' => count($data),
      ]),
      '#header' => $header,
      '#rows' => $data,
    ];
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
