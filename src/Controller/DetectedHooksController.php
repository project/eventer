<?php

namespace Drupal\eventer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Class DetectedHooksController.
 */
class DetectedHooksController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Cache\ChainedFastBackend definition.
   *
   * @var \Drupal\Core\Cache\ChainedFastBackend
   */
  protected $cacheBootstrap;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->cacheBootstrap = $container->get('cache.bootstrap');
    return $instance;
  }

  /**
   * List.
   *
   * @return array
   *   Return Hello string.
   */
  public function list() {
    // Load cached data.
    $detected = [];
    if ($hooks = $this->cacheBootstrap->get('eventer_detected_hooks')) {
      $detected = $hooks->data;
    }

    $header = ['Hook', 'Implementations'];
    $data = [];
    foreach ($detected as $hook => $implementations) {
      $imps = implode('<br>', array_keys($implementations));
      $data[] = [$hook, new FormattableMarkup($imps, [])];
    }

    $output[] = [
      '#theme' => 'table',
      // '#cache' => ['disabled' => TRUE],
      '#caption' => $this->t('This list is build from cache and we detected @cnt hooks.', [
        '@cnt' => count($data),
      ]),
      '#header' => $header,
      '#rows' => $data,
    ];
    return $output;
  }

}
