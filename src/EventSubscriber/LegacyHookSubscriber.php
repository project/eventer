<?php

namespace Drupal\eventer\EventSubscriber;

use Drupal\eventer\Event\AlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\eventer\Event\HookEvent;

/**
 * Hook Eventer event subscriber.
 */
class LegacyHookSubscriber implements EventSubscriberInterface {

  /**
   * Constructs event subscriber.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // Module handler decorator will define the events to listen.
    ];
  }

  /**
   * Hook request event handler.
   *
   * @param \Drupal\eventer\Event\HookEvent $event
   *   Hook event.
   */
  public function execute(HookEvent $event) {
    $result = call_user_func_array($event->getFunction(), $event->getParams());
    $event->setResult($result);
  }

  /**
   * Alter request event handler.
   *
   * @param \Drupal\eventer\Event\AlterEvent $event
   *   Alter event.
   */
  public function alter(AlterEvent $event) {
    $function = $event->getFunction();
    $data = $event->getData();
    $context1 = $event->getContext1();
    $context2 = $event->getContext2();

    $function($data, $context1, $context2);

    $event->setData($data);
    $event->setContext1($context1);
    $event->setContext2($context2);
  }

}
