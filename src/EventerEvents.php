<?php

namespace Drupal\eventer;

/**
 * Contains all events patterns in the eventer module.
 *
 * The final event name must be PATTERN.hook.
 */
final class EventerEvents {

  /**
   * The MODULE_HOOK event occurs when a hook is called.
   *
   * This is a event name pattern.
   *
   * This event pattern allows replace module hook calls.
   */
  const MODULE_HOOK = 'eventer.module.';

  /**
   * The MODULE_HOOK_ALTER event occurs when a hook alter is called.
   *
   * This is a event name pattern.
   *
   * This event pattern allows replace module hook alter calls.
   */
  const MODULE_HOOK_ALTER = 'eventer.module.alter.';

  /**
   * The THEME_HOOK_ALTER event occurs when a hook alter is called.
   *
   * This is a event name pattern.
   *
   * This event pattern allows replace theme hook alter calls.
   */
  const THEME_HOOK_ALTER = 'eventer.theme.alter.';

}
